"use strict";

// file: __form.js
function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}
waitForLoad(".widget-type-form,.widget-type-blog_content,.widget-type-blog_subscribe", "form", formError);

function formError() {
    $("form input, form select, form textarea").change(function () {
        var $this = $(this);
        setTimeout(function () {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().removeClass("error-border");
                $this.parent().addClass("verify");
            }
        }, 300);
    });

    $("form input, form select, form textarea").focusout(function (e) {
        var $this = $(this);
        setTimeout(function () {
            var error = $this.parent().next();
            if (error.hasClass("hs-error-msgs") && error != undefined) {
                $this.parent().addClass("error-border");
                $this.parent().removeClass("verify");
            } else {
                $this.parent().addClass("verify");
                $this.parent().removeClass("error-border");
            }
        }, 300);
    });
}

// Functions for dropdown
function select(wrapper, form) {
    form.find("select").each(function () {
        var parent = $(this).parent();
        if (parent.hasClass("dropdown_select")) return;
        parent.addClass('dropdown_select');
        parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
        parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
        $(this).find('option').each(function () {
            if ($(this).val() != "") {
                parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
            }
        });

        $(parent).find('.dropdown-header').click(function (event) {
            $(this).toggleClass('slide-down').next().slideToggle();
            $(this).children('.arrow-white').toggle();
        });
        $(parent).find('.dropdown-list li').click(function () {
            var choose = $(this).text();
            $(this).parent().siblings('.dropdown-header').find('span').text(choose);
            $(this).parent().siblings('select').find('option').removeAttr('selected');
            $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
            $(this).parent().find('li').removeClass('selected');
            $(this).addClass('selected');
            $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
            $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
        });
        $(parent).find('.dropdown_select .input').click(function (event) {
            event.stopPropagation();
        });
    });
}

//file upload
function fileUpload() {
    $('input[type=file]').each(function (i, e) {
        $(this).parent().addClass('file-upload');
        $(this).parent().append('<div class="overlay"><span>Drop file here</span></div>');
        $(this).parent().find('.overlay').append('<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="-929 271 60 60" style="enable-background:new -929 271 60 60;" xml:space="preserve"><g><path d="M-884,296h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,296-884,296z"/><path d="M-909,290h10c0.6,0,1-0.4,1-1s-0.4-1-1-1h-10c-0.6,0-1,0.4-1,1S-909.6,290-909,290z"/><path d="M-884,304h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,304-884,304z"/><path d="M-884,312h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,312-884,312z"/><path d="M-884,320h-25c-0.6,0-1,0.4-1,1s0.4,1,1,1h25c0.6,0,1-0.4,1-1S-883.4,320-884,320z"/><path d="M-880,285.6V271h-43v55h5v5h43v-40.4L-880,285.6z M-889,279.4l9,9l1.6,1.6H-889V279.4z M-921,324v-51h39v10.6l-7.6-7.6H-918v48H-921z M-916,329v-3v-48h25v14h14v37H-916z"/></g></svg>');
        $(this).change(function () {
            $(e).siblings(".overlay").find("span").text($(e).val().split("\\")[2]);
        });
    });
}

function hideEmptyLabel() {
    $('form label').each(function (i, e) {
        if ($(this).text() == "*") {
            $(this).addClass('hidden-label');
        }
    });
}

var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
if (iOS) {
    $('body').addClass('ios');
}
var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
if (isIE11) {
    console.log('IE11');
    $('body').addClass('ie11');
}
// true on IE11
// false on Edge and other IEs/browsers.

waitForLoad(".form, .widget-type-form, .widget-type-blog_content, .hs_cos_wrapper_type_form, .hs_cos_wrapper_type_blog_subscribe", "form", hideEmptyLabel);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", fileUpload);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", formError);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", function () {
    $('form select').on('change', function () {
        $('form .dropdown-header').addClass('dropdown-selected');
    });
});
// end file: __form.js

// file: __header.js
function changeHeaderColor() {
    var header = $('.header');
    var logoSrc = getLogoUrl();

    $('.logo img').attr('srcset', '');

    $(window).on("scroll", function () {
        var logo = $('.logo img'),
            scrollPosition = $(window).scrollTop(),
            textPosition = logoSrc.indexOf('incite_logo'),
            logoUrl = logoSrc.substr(0, textPosition),
            newLogoSrc = logoUrl + 'incite_logo_red.png';

        if (scrollPosition > 0) {
            header.addClass('header-color');
            logo.attr('src', newLogoSrc);
        } else {
            header.removeClass('header-color');
            logo.attr('src', logoSrc);
        }
    });
}

function getLogoUrl() {
    return $('.logo img').attr('src');
}

function menuMobileInit() {
    $('header.header .menu-container span div > ul').slicknav({
        prependTo: 'header.header .header-wrapper',
        label: "",
        duration: 500,
        init: function init() {
            var src = getLogoUrl();
            var logo = $("<img></img>").attr('src', src);
            var cta = $('.header-cta');
            logo.insertBefore(".slicknav_btn");

            cta.clone().insertAfter(".slicknav_btn");
        }
    });

    $('.slicknav_btn').click(function () {
        if ($(this).hasClass('slicknav_open')) {
            $('body').css({ 'overflow': 'hidden' });
        } else {
            $('body').css({ 'overflow': '' });
        }
    });
}

menuMobileInit();
changeHeaderColor();
// end file: __header.js

// file: slick_inits.js
// Homepage slider
$('.home-slider-wrapper > span').slick({
    dots: true,
    arrows: true
});
$('.solution-slider > span').on('init', function (event, slick) {
    $(event.target).find(".slick-slide[data-slick-index='2']").addClass("active-slide");
});
$('.solution-slider > span').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    responsive: [{
        breakpoint: 901,
        settings: {
            slidesToShow: 1,
            slidesToSCroll: 1
        }
    }]
});
$('.solution-slider > span').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

    if (slick.slideCount - 1 == currentSlide && nextSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 3) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 4) + "']").addClass("slide-upper");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 2) + "']").addClass("slide-upper");
    } else if (slick.slideCount - 1 == nextSlide && currentSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 1) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 2) + "']").addClass("slide-upper");
        $(event.target).find(".slick-slide[data-slick-index='" + currentSlide + "']").addClass("slide-upper");
    } else {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
    }
});
$('.solution-slider > span').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    $(event.target).find(".slick-slide").removeClass("slide-upper");
});
$('.slider .hs_cos_wrapper_type_widget_container').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
    arrows: true,
    dots: false,
    loop: true,
    responsive: [{
        breakpoint: 768,
        settings: {
            arrows: false,
            slidesToShow: 1
        }
    }]
});

// end file: slick_inits.js

// file: Modules/client-logos.js
$(document).ready(function () {
    moveClassesToWrapper();
    toggleList();
    swapTextOnClick();
    createSliderItems();
    modal();
});

function moveClassesToWrapper() {
    $('.clients-logos').find('.client-logo').each(function () {
        var logo = $(this);
        var wrapper = logo.parent();
        var logoClass = logo.attr('class');
        wrapper.addClass(logoClass);
        logo.attr('class', '');
    });
}

function toggleList() {
    $('.choose-filter').on('click', function () {
        $('.industries').slideToggle(300);
    });
}

function swapTextOnClick() {
    var button = $(".choose-filter");

    var $grid = $('.clients-logos > span').isotope({
        itemSelector: '.clients-logos > span > div',
        layoutMode: 'fitRows'
    });

    $('.industries .industry').on('click', function () {
        var listItem = $(this),
            topText = button.text(),
            listText = $(this).text(),
            filterClass = ('.' + listText.toLowerCase()).split(' ').join('-').split(',').join('');

        button.text(listText);
        listItem.text(topText);

        $grid.isotope({
            filter: filterClass
        });
        $('.industries').slideToggle(300);
    });
}

function createSliderItems() {
    var container = $('#modal-slider');

    $('.clients-logos').find('.client-logo').each(function (index) {

        var logo = $(this),
            button = logo.children('div').attr('data-button'),
            logoSrc = logo.find('img').attr('src'),
            text = logo.children('div').attr('data-text'),
            caseStudyUrl = logo.children('div').attr('data-url');

        logo.attr('data-index', index);
        var element = void 0;

        if (button == "true") {
            element = "\n                     <div class=\"modal-slider-item\">\n                        <img src=\"" + logoSrc + "\">\n                        <p>" + text + "</p>\n                        <a href=\"" + caseStudyUrl + "\">view case study</a>\n                    </div>\n                ";
        } else {
            element = "\n                    <div class=\"modal-slider-item\">\n                        <img src=\"" + logoSrc + "\">\n                        <p>" + text + "</p>\n                    </div>\n                ";
        }

        container.append(element);
    });
}

function initSlider(slideIndex) {
    $('#modal-slider').slick({
        autoplay: false,
        arrows: true,
        initialSlide: slideIndex,
        prevArrow: '<i class="fa fa-angle-left prev-arrow" aria-hidden="true"></i>',
        nextArrow: '<i class="fa fa-angle-right next-arrow" aria-hidden="true"></i>',
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false
            }
        }]
    });
}

function modal() {
    var modal = $('.modal');

    $('.clients-logos .client-logo').on('click', function () {
        var slideIndex = $(this).attr('data-index');

        modal.fadeIn(300);
        $('body').css({ 'overflow': 'hidden' });
        initSlider(parseInt(slideIndex));
    });

    $(".close-button > i").on('click', function (e) {
        //e.stopPropagation();
        modal.fadeOut(300);
        setTimeout(function () {
            $('#modal-slider').slick("unslick");
        }, 400);
        $('body').css({ 'overflow': '' });
    });

    $(".modal").on('click', function (e) {
        modal.fadeOut(300);

        setTimeout(function () {
            $('#modal-slider').slick("unslick");
        }, 400);
        $('body').css({ 'overflow': '' });
    });

    $(".modal-wrapper").on('click', function (e) {
        e.stopPropagation();
    });
}
// end file: Modules/client-logos.js

// file: Modules/latest-tweet.js
function withdrawTweet(twitterIframe) {
    var tweetIndex = 0,
        tweetWrap = $(twitterIframe),
        tweetIframe = tweetWrap.find('iframe'),
        tweetWrapper = tweetIframe.contents().find('.timeline-TweetList-tweet'),
        tweetCustom = tweetWrap.find('.custom-tweet');
    console.log(tweetWrap);
    console.log(tweetWrapper);
    console.log(tweetCustom);

    tweetWrapper.each(function () {
        var tweetText = $(this).contents().find('.timeline-Tweet-text').html(),
            tweetDate = $(this).contents().find('.timeline-Tweet-metadata time').attr('datetime').split(/T|\+|\-|\:/),
            tweetAvatarSrc = $(this).contents().find('.Avatar.Avatar--edge').attr('src'),
            tweetName = $(this).contents().find('.TweetAuthor-name').html(),
            tweetNameShort = $(this).contents().find('.TweetAuthor-screenName').html(),
            tweetDateString = tweetDate[1] + '/' + tweetDate[2] + '/' + tweetDate[0][2] + tweetDate[0][3] + ', ' + tweetDate[3] + ':' + tweetDate[4];
        // tweetCustom.eq(tweetIndex).find('.tweet-name').html(tweetName);
        // tweetCustom.eq(tweetIndex).find('.tweet-name-short').html(tweetNameShort);
        tweetCustom.eq(tweetIndex).find('.text-wrapper p').html(tweetText);
        // tweetCustom.eq(tweetIndex).find('.tweet-date p').html(tweetDateString);
        // tweetCustom.eq(tweetIndex).find('.tweet-avatar img').attr('src', tweetAvatarSrc);

        // tweetIndex++;
    });
    tweetWrap.find('.loader').fadeOut();
}

$(window).load(function () {
    setSameHeight('.latest-box .text-wrapper');
    var interval = setInterval(function () {
        if ($('.twitter-wrap iframe').contents().find('.timeline-TweetList-tweet').length > 0) {
            clearInterval(interval);
            withdrawTweet('.twitter-box');
        }
    }, 50);
});

function setSameHeight(elementsToMatch) {
    var el = $(elementsToMatch);
    var maxHeight = 0;

    el.map(function (i, e) {
        var thisH = $(e).innerHeight();
        if (thisH > maxHeight) {
            maxHeight = thisH;
        }
    });

    el.css('height', maxHeight);
}
// end file: Modules/latest-tweet.js

// file: Modules/list.js
function questionAccordion() {
    if ($("html.hs-inline-edit").length == 0) {
        $("section.question").each(function (i, e) {
            $(e).find(".panel-group").append($(e).find(".faq-box"));
            $(e).find(".hs_cos_wrapper_type_widget_container,.hs_cos_wrapper_type_custom_widget").remove();
        });
    }
    $("section.question .panel-group").attr("id", "accordion");
    $("section.question .panel-group .panel").each(function (i, e) {
        var href = $(e).find("h4.panel-title > a").attr("href");
        $(e).find("h4.panel-title > a").attr("href", href + (i + 1));

        var id = $(e).find(".panel-collapse").attr("id");
        $(e).find(".panel-collapse").attr("id", id + (i + 1));
    });
}

$().ready(questionAccordion);
// end file: Modules/list.js
//# sourceMappingURL=template.js.map
