function changeHeaderColor() {
    var header = $('.header');
    var logoSrc = getLogoUrl();

    $('.logo img').attr('srcset','');

    $(window).on("scroll", function() {
        var logo = $('.logo img'),
            scrollPosition = $(window).scrollTop(),
            textPosition = logoSrc.indexOf('incite_logo'),
            logoUrl = logoSrc.substr(0, textPosition),
            newLogoSrc = logoUrl + 'incite_logo_red.png';

        if (scrollPosition > 0) {
            header.addClass('header-color');
            logo.attr('src', newLogoSrc);
        } else {
            header.removeClass('header-color');
            logo.attr('src', logoSrc);
        }
    })
}

function getLogoUrl() {
    return $('.logo img').attr('src');
}

function menuMobileInit() {
    $('header.header .menu-container span div > ul').slicknav({
        prependTo: 'header.header .header-wrapper',
        label: "",
        duration: 500,
        init: function() {
            var src = getLogoUrl();
            var logo = $("<img></img>").attr('src', src);
            var cta = $('.header-cta');
            logo.insertBefore(".slicknav_btn");

            cta.clone().insertAfter(".slicknav_btn");
        }
    });

    $('.slicknav_btn').click(function() {
        if ($(this).hasClass('slicknav_open')) {
            $('body').css({ 'overflow' : 'hidden'});
        } else {
            $('body').css({ 'overflow' : ''});
        }
    })
    

}

menuMobileInit();
changeHeaderColor();