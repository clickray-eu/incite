$(document).ready(function () {
    moveClassesToWrapper();
    toggleList();
    swapTextOnClick();
    createSliderItems();
    modal();
})

function moveClassesToWrapper() {
    $('.clients-logos').find('.client-logo').each(function () {
        var logo = $(this);
        var wrapper = logo.parent()
        var logoClass = logo.attr('class');
        wrapper.addClass(logoClass);
        logo.attr('class', '');

    })
}

function toggleList() {
    $('.choose-filter').on('click', function () {
        $('.industries').slideToggle(300);
    })
}

function swapTextOnClick() {
    var button = $(".choose-filter");

    var $grid = $('.clients-logos > span').isotope({
        itemSelector: '.clients-logos > span > div',
        layoutMode: 'fitRows'
    });

    $('.industries .industry').on('click', function () {
        var listItem = $(this),
            topText = button.text(),
            listText = $(this).text(),
            filterClass = ('.' + listText.toLowerCase()).split(' ').join('-').split(',').join('');

        button.text(listText);
        listItem.text(topText);

        $grid.isotope({
            filter: filterClass
        });
        $('.industries').slideToggle(300);
    })
}


function createSliderItems() {
    let container = $('#modal-slider');

    $('.clients-logos').find('.client-logo').each( function(index) {

            let logo = $(this),
                button = logo.children('div').attr('data-button'),
                logoSrc = logo.find('img').attr('src'),
                text = logo.children('div').attr('data-text'),
                caseStudyUrl = logo.children('div').attr('data-url');

            logo.attr('data-index', index);
            let element;

            if (button == "true") {
                element = `
                     <div class="modal-slider-item">
                        <img src="${logoSrc}">
                        <p>${text}</p>
                        <a href="${caseStudyUrl}">view case study</a>
                    </div>
                `;
            } else {
                element = `
                    <div class="modal-slider-item">
                        <img src="${logoSrc}">
                        <p>${text}</p>
                    </div>
                `;
            }

            container.append(element);
                
    })
 }

 function initSlider(slideIndex) {
     $('#modal-slider').slick({
         autoplay: false,
         arrows: true,
         initialSlide: slideIndex,
         prevArrow: '<i class="fa fa-angle-left prev-arrow" aria-hidden="true"></i>',
         nextArrow: '<i class="fa fa-angle-right next-arrow" aria-hidden="true"></i>',
         adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 768,
                    settings: {
                        arrows: false
                    }
                }
            ]
        });
 }

 function modal() {
     let modal = $('.modal');

     $('.clients-logos .client-logo').on('click', function() {
        let slideIndex = $(this).attr('data-index');

        modal.fadeIn(300);
        $('body').css({'overflow':'hidden'});
        initSlider(parseInt(slideIndex));
     })

     $(".close-button > i").on('click', function(e){ 
         //e.stopPropagation();
         modal.fadeOut(300);
         setTimeout(() => {
            $('#modal-slider').slick("unslick");
        }, 400)
         $('body').css({'overflow':''});
     })


     $(".modal").on('click', function(e){ 
         modal.fadeOut(300);
         
         setTimeout(() => {
            $('#modal-slider').slick("unslick");
        }, 400)
         $('body').css({'overflow':''});

     })

    $(".modal-wrapper").on('click', function(e){ 
         e.stopPropagation();
    })
 }