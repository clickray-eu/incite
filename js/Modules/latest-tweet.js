function withdrawTweet(twitterIframe) {
    var tweetIndex = 0,
        tweetWrap = $(twitterIframe),
        tweetIframe = tweetWrap.find('iframe'),
        tweetWrapper = tweetIframe.contents().find('.timeline-TweetList-tweet'),
        tweetCustom = tweetWrap.find('.custom-tweet');
        console.log(tweetWrap);
        console.log(tweetWrapper);
        console.log(tweetCustom);

    tweetWrapper.each(function () {
        var tweetText = $(this).contents().find('.timeline-Tweet-text').html(),
            tweetDate = $(this).contents().find('.timeline-Tweet-metadata time').attr('datetime').split(/T|\+|\-|\:/),
            tweetAvatarSrc = $(this).contents().find('.Avatar.Avatar--edge').attr('src'),
            tweetName = $(this).contents().find('.TweetAuthor-name').html(),
            tweetNameShort = $(this).contents().find('.TweetAuthor-screenName').html(),
            tweetDateString = tweetDate[1] + '/' + tweetDate[2] + '/' + tweetDate[0][2] + tweetDate[0][3]   + ', ' + tweetDate[3] + ':' + tweetDate[4];
        // tweetCustom.eq(tweetIndex).find('.tweet-name').html(tweetName);
        // tweetCustom.eq(tweetIndex).find('.tweet-name-short').html(tweetNameShort);
        tweetCustom.eq(tweetIndex).find('.text-wrapper p').html(tweetText);
        // tweetCustom.eq(tweetIndex).find('.tweet-date p').html(tweetDateString);
        // tweetCustom.eq(tweetIndex).find('.tweet-avatar img').attr('src', tweetAvatarSrc);

        // tweetIndex++;
    })
    tweetWrap.find('.loader').fadeOut();
}

$(window).load(function () {
    setSameHeight('.latest-box .text-wrapper');
    var interval = setInterval(function () {
        if ($('.twitter-wrap iframe').contents().find('.timeline-TweetList-tweet').length > 0) {
            clearInterval(interval);
            withdrawTweet('.twitter-box');
        }
    }, 50);
})

function setSameHeight(elementsToMatch) {
	let el = $(elementsToMatch);
	let maxHeight = 0;

	el.map((i, e) => {
		let thisH = $(e).innerHeight();
		if (thisH > maxHeight) { maxHeight = thisH; }
	})

	el.css('height', maxHeight);

}