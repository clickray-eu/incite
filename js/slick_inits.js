// Homepage slider
$('.home-slider-wrapper > span').slick({
    dots: true,
    arrows: true
});
$('.solution-slider > span').on('init', function (event, slick) {
    $(event.target).find(".slick-slide[data-slick-index='2']").addClass("active-slide");
});
$('.solution-slider > span').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    arrows: true,
    responsive: [
        {
            breakpoint: 901,
            settings: {
                slidesToShow: 1,
                slidesToSCroll: 1
            }
        }
    ]
});
$('.solution-slider > span').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

    if (slick.slideCount - 1 == currentSlide && nextSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 3) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 4) + "']").addClass("slide-upper");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 2) + "']").addClass("slide-upper");
        
    } else if (slick.slideCount -1 == nextSlide && currentSlide == 0) {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide +1) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide + 2) + "']").addClass("slide-upper");
        $(event.target).find(".slick-slide[data-slick-index='" + (currentSlide) + "']").addClass("slide-upper");

    } else {
        $(event.target).find(".slick-slide").removeClass("active-slide");
        $(event.target).find(".slick-slide[data-slick-index='" + (nextSlide + 2) + "']").addClass("active-slide");
    }
});
$('.solution-slider > span').on('afterChange', function (event, slick, currentSlide, nextSlide) {
    $(event.target).find(".slick-slide").removeClass("slide-upper");
});
$('.slider .hs_cos_wrapper_type_widget_container').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 10000,
    arrows: true,
    dots: false,
    loop: true,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1
            }
        }
    ]
});
